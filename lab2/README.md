# Laboratory work #2

## Task

[link](https://docs.google.com/document/d/1IhDd-hYARm0Vtsj-T-O3uiw-tDi19DFk7Kd0DAf8DnQ) | [pdf](src/main/resources/docs/Java._Sem_1._Assignment_2.pdf)

## Model

### Basic model

![model](src/main/resources/img/model.png)

### Entity Relationship diagram

![ER diagram](src/main/resources/img/ER_diagram.png)

Choosen strategy: **joined**

### Description of model

| Relation | Attribute | Type (size) |
|------------|---------|--------------------|
| **Invoices** <br> Contains information about transport invoices | <span style="color:red">_id_</span> — unique invoice number <br> _date_departure_ — the date of registration and departure of the bundle <br> _date_arrival_ — date of bundle arrival. Allows NULL <br> _shipping_cost_ — shipping cost (UAH) <br> <span style="color:blue">_sender_id_</span> — sender ID <br> <span style="color:blue">_recipient_id_</span> — recipient ID  <br> <span style="color:blue">_warehouse_dep_id_</span> — the number of warehouse from which the bundle was sent<br> <span style="color:blue">_warehouse_arr_id_</span> — the number of the warehouse to which the bundle goes | Number <br> Date <br> Date <br> Money <br> Number <br> Number <br> Number <br> Number <br>  |
| **Goods** <br> Contains information about goods in one transport invoice. Abstaract model | <span style="color:red">_id_</span> — unique identifier <br> _weight_ — weight of bundle (kg) <br> _insured_val_ — insured value of cargo (UAH) <br>  <span style="color:blue">_invoice_id_</span> — the invoice number to which the bundle belongs | Number <br> Float <br> Number <br> Number |
| **Tires** <br> Special type of goods for automobile tires | <span style="color:red">_id_</span> — unique identifier <br> _radius_ — radius of wheel <br> _for_lorry_ — if true - tires for truck, for car - otherwise <br> | Number <br> Number <br> Boolean |
| **Parcel** <br> Special type of goods, represents a small box of something | <span style="color:red">_id_</span> — unique identifier <br> _height_ — height of parcel (mm) <br> _width_ — width of parcel (mm) <br> _depth_ — depth of parcel (mm) <br>  _description_ — description of things inside box | Number <br> Number <br> Number <br> Number <br> Text (255) |
| **Contragents** <br> Contains information about persons who are senders or receivers | <span style="color:red">_id_</span> — person's tax identification number (IDN) <br> _name_ — name of person <br> _phone_number_ — person's mobile phone number | Number <br> Text (255) <br> Text (15) |
| **Warehouses** <br> Contains information about the warehouses between which the goods are transported | <span style="color:red">_id_</span> — unique warehouse number <br> _address_ — address, where the warehouse is located<br> _phone_number_ — help desk phone number | Number <br> Text (255) <br> Text (15) |
