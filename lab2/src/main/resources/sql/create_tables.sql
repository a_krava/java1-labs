CREATE TABLE IF NOT EXISTS contragents
(
    id           serial PRIMARY KEY,
    name         text NOT NULL,
    phone_number varchar(15) NOT NULL
);

CREATE TABLE IF NOT EXISTS warehouses
(
    id           serial PRIMARY KEY,
    address      text NOT NULL,
    phone_number varchar(15) NOT NULL
);

CREATE TABLE IF NOT EXISTS invoices
(
    id               serial PRIMARY KEY,
    date_departure   date NOT NULL,
    date_arrival     date,
    shipping_cost    money NOT NULL,
    sender_id        serial NOT NULL,
    recipient_id     serial NOT NULL,
    warehouse_dep_id serial NOT NULL,
    warehouse_arr_id serial NOT NULL,
    CONSTRAINT recipient_id FOREIGN KEY (recipient_id) REFERENCES contragents (id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT,
    CONSTRAINT sender_id FOREIGN KEY (sender_id) REFERENCES contragents (id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT,
    CONSTRAINT warehouse_arr_id FOREIGN KEY (warehouse_arr_id) REFERENCES warehouses (id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT,
    CONSTRAINT warehouse_dep_id FOREIGN KEY (warehouse_dep_id) REFERENCES warehouses (id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS goods
(
    id          serial PRIMARY KEY,
    weight      float NOT NULL,
    insured_val money NOT NULL,
    invoice_id  serial NOT NULL,
    CONSTRAINT invoice_id FOREIGN KEY (invoice_id) REFERENCES invoices (id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS parcels
(
    id          serial PRIMARY KEY,
    height      int NOT NULL,
    width       int NOT NULL,
    depth       int NOT NULL,
    description text,
    CONSTRAINT id FOREIGN KEY (id) REFERENCES goods (id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS tires
(
    id        serial PRIMARY KEY,
    radius    int NOT NULL,
    for_lorry bool NOT NULL,
    CONSTRAINT id FOREIGN KEY (id)  REFERENCES goods (id)
        ON UPDATE RESTRICT
        ON DELETE RESTRICT
);