package ua.kpi.fpm.pzks.model;

import ua.kpi.fpm.pzks.service.Column;
import ua.kpi.fpm.pzks.service.Entity;
import ua.kpi.fpm.pzks.service.InheritedEntity;

import java.math.BigDecimal;

@Entity(table = "tires")
@InheritedEntity
public class Tires extends Goods {
    @Column
    private int radius;
    @Column(name = "for_lorry")
    private boolean forLorry;

    public Tires() { }

    public Tires(long id, double weight, BigDecimal insuredValue,
                 long invoiceId, int radius, boolean forLorry) {
        super(id, weight, insuredValue, invoiceId);
        this.radius = radius;
        this.forLorry = forLorry;
    }

    public Tires(long id, double weight, double insuredValue,
                 long invoiceId, int radius, boolean forLorry) {
        this(id, weight, new BigDecimal(insuredValue), invoiceId, radius, forLorry);
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public boolean isForLorry() {
        return forLorry;
    }

    public void setForLorry(boolean forLorry) {
        this.forLorry = forLorry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Tires tires = (Tires) o;
        return radius == tires.radius &&
                forLorry == tires.forLorry;
    }

    @Override
    public String toString() {
        return "Tires{" +
                "id=" + this.getId() +
                ", weight=" + this.getWeight() +
                ", insuredValue=" + this.getInsuredValue() +
                ", invoiceId=" + this.getInvoiceId() +
                ", radius=" + radius +
                ", forLorry=" + forLorry +
                '}';
    }
}
