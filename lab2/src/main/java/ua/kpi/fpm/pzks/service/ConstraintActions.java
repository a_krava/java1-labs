package ua.kpi.fpm.pzks.service;

public enum ConstraintActions {
    CASCADE,
    NO_ACTION,
    RESTRICT,
    SET_DEFAULT,
    SET_NULL;

    @Override
    public String toString() {
        return super.toString().replace('_', ' ');
    }
}
