package ua.kpi.fpm.pzks.dao;

import org.javatuples.Pair;
import org.postgresql.ds.PGSimpleDataSource;
import ua.kpi.fpm.pzks.service.Column;
import ua.kpi.fpm.pzks.service.Entity;
import ua.kpi.fpm.pzks.service.InheritedEntity;
import ua.kpi.fpm.pzks.service.Primary;

import java.io.InputStream;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class DAOImpl<T> implements IDAOImpl<T> {
    private Class<T> clazz;
    private Connection connection;

    public DAOImpl(Class<T> clazz) throws Exception {
        this.clazz = clazz;
        Properties properties = new Properties();
        var classloader = Thread.currentThread().getContextClassLoader();
        InputStream inputStream = classloader.getResourceAsStream("db/db.properties");
        if (inputStream == null) {
            throw new Exception("Couldn't read db.properties");
        }
        properties.load(inputStream);
        var pgDataSource = new PGSimpleDataSource();
        pgDataSource.setServerName(properties.getProperty("DB_HOST"));
        pgDataSource.setDatabaseName(properties.getProperty("DB_NAME"));
        pgDataSource.setPortNumber(Integer.parseInt(properties.getProperty("DB_PORT")));
        pgDataSource.setUser(properties.getProperty("DB_USERNAME"));
        pgDataSource.setPassword(properties.getProperty("DB_PASSWORD"));
        connection = pgDataSource.getConnection();
    }

    public DAOImpl(Class<T> clazz, Connection connection) {
        this.clazz = clazz;
        this.connection = connection;
    }

    /*
    @Override
    public boolean createEntity(T entity) {
        return false;
    }
    */

    private static void checkClassIsEntity(Class<?> clazz) throws Exception {
        if (!clazz.isAnnotationPresent(Entity.class)) {
            throw new Exception(String.format("Class %s is not a table", clazz.getName()));
        }
    }

    private static String getTableName(Class<?> clazz) throws Exception {
        checkClassIsEntity(clazz);
        return clazz.getAnnotation(Entity.class).table();
    }

    private static boolean isEntityInherited(Class<?> clazz) throws Exception {
        checkClassIsEntity(clazz);
        if (!clazz.isAnnotationPresent(InheritedEntity.class)) {
            return false;
        }
        var baseEntity = clazz.getSuperclass();
        if (!baseEntity.isAnnotationPresent(Entity.class)) {
            throw new Exception(String.format("Base class %s is not a table for inherited %s",
                    baseEntity.getName(), clazz.getName()));
        }
        return true;
    }

    private static String getColumnName(Field field) {
        var annotation = field.getAnnotation(Column.class);
        if (annotation.name().isEmpty()) {
            return field.getName();
        } else {
            return annotation.name();
        }
    }

    private static Pair<Field, Field[]> getAllColumns(Class<?> clazz) throws Exception {
        Field[] fields = clazz.getDeclaredFields();
        List<Field> listColumns = new ArrayList<>();
        Field primary = null;
        for (var field : fields) {
            if (field.isAnnotationPresent(Column.class)) {
                if (field.isAnnotationPresent(Primary.class)) {
                    if (primary != null) {
                        System.err.println("Expected one primary key, but got more:");
                        System.err.printf("First was: %s, second: %s in class: %s%n",
                                primary.getName(), field.getName(), clazz.getName());
                        throw new Exception("Too many primary fields");
                    }
                    primary = field;
                } else {
                    listColumns.add(field);
                }
            }
        }
        if (primary == null && !isEntityInherited(clazz)) {
            throw new Exception(String.format("No primary field found in %s", clazz.getName()));
        } else if (primary != null && isEntityInherited(clazz)) {
            throw new Exception(String.format("Inherited class %s shouldn't have primary key", clazz.getName()));
        }
        return new Pair<>(primary, listColumns.toArray(new Field[0]));
    }

    private static String[] getAllColumnNames(Field[] columns) {
        String[] columnNames = new String[columns.length];
        for (int i = 0; i < columns.length; i++) {
            columnNames[i] = getColumnName(columns[i]);
        }
        return columnNames;
    }

    private static String buildSelectQuery(Class<?> clazz, Long id) throws Exception {
        StringBuilder query = new StringBuilder("SELECT ");
        String tableName = getTableName(clazz);
        query.append(buildSelectQueryForColumns(clazz, id != null));
        query.append("FROM ");
        query.append(tableName);
        query.append(' ');
        ArrayList<String> inheritedTableNames = new ArrayList<>();
        while (isEntityInherited(clazz)) {
            clazz = clazz.getSuperclass();
            inheritedTableNames.add(getTableName(clazz));
        }
        String primaryKey = getColumnName(getAllColumns(clazz).getValue0());
        if (!inheritedTableNames.isEmpty()) {
            inheritedTableNames.add(0, tableName);
            query.append("INNER JOIN ");
            for (int i = 1; i < inheritedTableNames.size(); i++) {
                query.append(inheritedTableNames.get(i));
                query.append(" on ");
                query.append(inheritedTableNames.get(i - 1));
                query.append('.');
                query.append(primaryKey);
                query.append(" = ");
                query.append(inheritedTableNames.get(i));
                query.append('.');
                query.append(primaryKey);
                query.append(' ');
            }
        }
        if (id != null) {
            query.append("WHERE ");
            query.append(tableName);
            query.append('.');
            query.append(primaryKey);
            query.append(" = ");
            query.append(id);
        }
        return query.toString();
    }

    private static String buildSelectQueryForColumns(Class<?> clazz, boolean getById) throws Exception {
        StringBuilder query = new StringBuilder();
        String tableName = getTableName(clazz);
        var result = getAllColumns(clazz);
        Field[] columns = result.getValue1();
        if (!getById && result.getValue0() != null) {
            var temp = columns;
            columns = new Field[columns.length + 1];
            System.arraycopy(temp, 0, columns, 0, temp.length);
            columns[temp.length] = result.getValue0();
        }
        String[] columnNames = getAllColumnNames(columns);
        boolean isInherited = isEntityInherited(clazz);
        for (int i = 0; i < columnNames.length; i++) {
            String fieldName = columns[i].getName();
            query.append(tableName);
            query.append('.');
            query.append(columnNames[i]);
            query.append(' ');
            query.append('"');
            query.append(fieldName);
            query.append('"');
            if (i != columnNames.length - 1) {
                query.append(", ");
            } else {
                query.append(' ');
            }
        }
        if (isInherited) {
            query.append(", ");
            query.append(buildSelectQueryForColumns(clazz.getSuperclass(), getById));
        }
        return query.toString();
    }

    private T createNewInstanceAndFillData(ResultSet resultSet, Long id) throws Exception {
        T item = clazz.getDeclaredConstructor().newInstance();
        List<Field> columns = new ArrayList<>();
        Class<? super T> currentClazz = clazz;
        Field primaryKey = null;
        while (true) {
            var result = getAllColumns(currentClazz);
            columns.addAll(Arrays.asList(result.getValue1()));
            if (isEntityInherited(currentClazz)) {
                currentClazz = currentClazz.getSuperclass();
            } else {
                if (id == null) {
                    columns.add(result.getValue0());
                } else {
                    primaryKey = result.getValue0();
                }
                break;
            }
        }
        for (var column : columns) {
            column.setAccessible(true);
            var value = resultSet.getObject(column.getName());
            column.set(item, value);
        }
        if (id != null) {
            primaryKey.setAccessible(true);
            primaryKey.set(item, id);
        }
        return item;
    }


    @Override
    public T getEntity(long id) {
        try {
            String query = buildSelectQuery(clazz, id);
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            if (!rs.next()) {
                return null;
            }
            return createNewInstanceAndFillData(rs, id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /*
    @Override
    public T updateEntity(T entity) {
        return null;
    }

    @Override
    public boolean deleteEntity(long id) {
        return false;
    }
    */

    @Override
    public List<T> getEntityList() {
        ArrayList<T> result = new ArrayList<>();
        try {
            String query = buildSelectQuery(clazz, null);
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while(rs.next()) {
                result.add(createNewInstanceAndFillData(rs, null));
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
