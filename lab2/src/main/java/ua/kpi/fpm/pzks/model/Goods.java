package ua.kpi.fpm.pzks.model;

import ua.kpi.fpm.pzks.service.Column;
import ua.kpi.fpm.pzks.service.Entity;
import ua.kpi.fpm.pzks.service.ForeignKey;
import ua.kpi.fpm.pzks.service.Primary;

import java.math.BigDecimal;
import java.util.Objects;

@Entity(table = "goods")
public abstract class Goods {
    @Column
    @Primary
    private long id;
    @Column
    private double weight;
    @Column(name = "insured_val")
    private BigDecimal insuredValue;
    @Column(name = "invoice_id")
    @ForeignKey(refTable = "invoices", refKey = "id")
    private long invoiceId;

    public Goods() { }

    public Goods(long id, double weight, BigDecimal insuredValue, long invoiceId) {
        this.id = id;
        this.weight = weight;
        this.insuredValue = insuredValue;
        this.invoiceId = invoiceId;
    }

    protected Goods(long id, double weight, double insuredValue, long invoiceId) {
        this(id, weight, new BigDecimal(insuredValue), invoiceId);
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public BigDecimal getInsuredValue() {
        return insuredValue;
    }

    public void setInsuredValue(BigDecimal insuredValue) {
        this.insuredValue = insuredValue;
    }

    public long getId() {
        return id;
    }

    public long getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(long invoiceId) {
        this.invoiceId = invoiceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Goods goods = (Goods) o;
        return id == goods.id &&
                Double.compare(goods.weight, weight) == 0 &&
                invoiceId == goods.invoiceId &&
                insuredValue.equals(goods.insuredValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Goods{" +
                "id=" + id +
                ", weight=" + weight +
                ", insuredValue=" + insuredValue +
                ", invoiceId=" + invoiceId +
                '}';
    }
}
