package ua.kpi.fpm.pzks.model;

import ua.kpi.fpm.pzks.service.Column;
import ua.kpi.fpm.pzks.service.Entity;
import ua.kpi.fpm.pzks.service.InheritedEntity;

import java.math.BigDecimal;
import java.util.Objects;

@Entity(table = "parcels")
@InheritedEntity
public class Parcel extends Goods {
    @Column
    private int height;
    @Column
    private int width;
    @Column
    private int depth;
    @Column(nullable = true)
    private String description;

    public Parcel() { }

    public Parcel(long id, double weight, BigDecimal insuredValue, long invoiceId, int height,
                  int width, int depth, String description) {
        super(id, weight, insuredValue, invoiceId);
        this.height = height;
        this.width = width;
        this.depth = depth;
        this.description = description;
    }

    public Parcel(long id, double weight, double insuredValue, long invoiceId, int height,
                  int width, int depth, String description) {
        this(id, weight, new BigDecimal(insuredValue), invoiceId, height, width, depth, description);
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Parcel parcel = (Parcel) o;
        return height == parcel.height &&
                width == parcel.width &&
                depth == parcel.depth &&
                Objects.equals(description, parcel.description);
    }

    @Override
    public String toString() {
        return "Parcel{" +
                "id=" + this.getId() +
                ", weight=" + this.getWeight() +
                ", insuredValue=" + this.getInsuredValue() +
                ", invoiceId=" + this.getInvoiceId() +
                ", height=" + height +
                ", width=" + width +
                ", depth=" + depth +
                ", description='" + description + '\'' +
                '}';
    }
}
