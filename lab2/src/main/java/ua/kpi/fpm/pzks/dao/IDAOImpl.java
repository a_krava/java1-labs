package ua.kpi.fpm.pzks.dao;

import java.util.List;

public interface IDAOImpl<T> {
    /*
    boolean createEntity(T entity);
    */
    T getEntity(long id);
    /*
    T updateEntity(T entity);
    boolean deleteEntity(long id);
    */
    List<T> getEntityList();
}
