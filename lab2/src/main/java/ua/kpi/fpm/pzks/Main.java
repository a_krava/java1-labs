package ua.kpi.fpm.pzks;

import ua.kpi.fpm.pzks.dao.DAOImpl;
import ua.kpi.fpm.pzks.model.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Lab 2");
        DAOImpl<Warehouse> warehouseDAO = null;
        DAOImpl<Contragent> contragentDAO = null;
        DAOImpl<Invoice> invoiceDAO = null;
        DAOImpl<Parcel> parcelDAO = null;
        DAOImpl<Tires> tiresDAO = null;
        try {
            warehouseDAO = new DAOImpl<>(Warehouse.class);
            contragentDAO = new DAOImpl<>(Contragent.class);
            invoiceDAO = new DAOImpl<>(Invoice.class);
            parcelDAO = new DAOImpl<>(Parcel.class);
            tiresDAO = new DAOImpl<>(Tires.class);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        showDAO(warehouseDAO, "Warehouses");
        showDAO(contragentDAO, "Contragents");
        showDAO(invoiceDAO, "Invoices");
        showDAO(parcelDAO, "Parcels");
        showDAO(tiresDAO, "Tires");
    }

    private static void showDAO(DAOImpl<?> dao, String name) {
        System.out.println(name);
        var itemId0 = dao.getEntity(0);
        var itemId13 = dao.getEntity(13);
        var list = dao.getEntityList();
        System.out.println(itemId0);
        System.out.println(itemId13);
        System.out.println(list);
    }

}
