package ua.kpi.fpm.pzks.model;

import ua.kpi.fpm.pzks.service.Column;
import ua.kpi.fpm.pzks.service.Entity;
import ua.kpi.fpm.pzks.service.Primary;

import java.util.Objects;

@Entity(table = "warehouses")
public class Warehouse {
    @Column
    @Primary
    private long id;
    @Column
    private String address;
    @Column(name = "phone_number")
    private String phoneNumber;

    public Warehouse() { }

    public Warehouse(long id, String address, String phoneNumber) {
        this.id = id;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Warehouse warehouse = (Warehouse) o;
        return id == warehouse.id &&
                address.equals(warehouse.address) &&
                phoneNumber.equals(warehouse.phoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Warehouse{" +
                "id=" + id +
                ", address='" + address + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
