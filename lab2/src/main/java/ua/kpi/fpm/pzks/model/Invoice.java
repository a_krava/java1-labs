package ua.kpi.fpm.pzks.model;

import ua.kpi.fpm.pzks.service.Column;
import ua.kpi.fpm.pzks.service.Entity;
import ua.kpi.fpm.pzks.service.ForeignKey;
import ua.kpi.fpm.pzks.service.Primary;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

@Entity(table = "invoices")
public class Invoice {
    @Column
    @Primary
    private long id;
    @Column(name = "date_departure")
    private Date departure;
    @Column(name = "date_arrival", nullable = true)
    private Date arrival;
    @Column(name = "shipping_cost")
    private BigDecimal shippingCost;
    @Column(name = "sender_id")
    @ForeignKey(refTable = "contragents", refKey = "id")
    private long senderId;
    @Column(name = "recipient_id")
    @ForeignKey(refTable = "contragents", refKey = "id")
    private long receiverId;
    @Column(name = "warehouse_dep_id")
    @ForeignKey(refTable = "warehouses", refKey = "id")
    private long warehouseDepartureId;
    @Column(name = "warehouse_arr_id")
    @ForeignKey(refTable = "warehouses", refKey = "id")
    private long warehouseArrivalId;

    public Invoice() { }

    public Invoice(long id, Date departure, Date arrival, BigDecimal shippingCost, long senderId,
                   long receiverId, long warehouseDepartureId, long warehouseArrivalId) {
        this.id = id;
        this.departure = departure;
        this.arrival = arrival;
        this.shippingCost = shippingCost;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.warehouseDepartureId = warehouseDepartureId;
        this.warehouseArrivalId = warehouseArrivalId;
    }

    public Invoice(long id, String departure, String arrival, double shippingCost, long senderId,
                   long receiverId, long warehouseDepartureId, long warehouseArrivalId) throws ParseException {
        this(id, new SimpleDateFormat("DD.MM.YY").parse(departure),
                arrival.isEmpty() ? null : new SimpleDateFormat().parse(arrival),
                new BigDecimal(shippingCost), senderId, receiverId,
                warehouseDepartureId, warehouseArrivalId);
    }

    public Date getDeparture() {
        return departure;
    }

    public void setDeparture(Date departure) {
        this.departure = departure;
    }

    public Date getArrival() {
        return arrival;
    }

    public void setArrival(Date arrival) {
        this.arrival = arrival;
    }

    public BigDecimal getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(BigDecimal shippingCost) {
        this.shippingCost = shippingCost;
    }

    public long getId() {
        return id;
    }

    public long getSenderId() {
        return senderId;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public long getWarehouseDepartureId() {
        return warehouseDepartureId;
    }

    public long getWarehouseArrivalId() {
        return warehouseArrivalId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public void setReceiverId(long receiverId) {
        this.receiverId = receiverId;
    }

    public void setWarehouseDepartureId(long warehouseDepartureId) {
        this.warehouseDepartureId = warehouseDepartureId;
    }

    public void setWarehouseArrivalId(long warehouseArrivalId) {
        this.warehouseArrivalId = warehouseArrivalId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Invoice invoice = (Invoice) o;
        return id == invoice.id &&
                senderId == invoice.senderId &&
                receiverId == invoice.receiverId &&
                warehouseDepartureId == invoice.warehouseDepartureId &&
                warehouseArrivalId == invoice.warehouseArrivalId &&
                departure.equals(invoice.departure) &&
                Objects.equals(arrival, invoice.arrival) &&
                shippingCost.equals(invoice.shippingCost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + id +
                ", departure=" + departure +
                ", arrival=" + arrival +
                ", shippingCost=" + shippingCost +
                ", senderId=" + senderId +
                ", receiverId=" + receiverId +
                ", warehouseDepartureId=" + warehouseDepartureId +
                ", warehouseArrivalId=" + warehouseArrivalId +
                '}';
    }
}
