package ua.kpi.fpm.pzks.dao;

import ua.kpi.fpm.pzks.model.*;

import java.util.List;

public class DAO {
    private IDAOImpl<Contragent> contragentIDAOImpl;
    private IDAOImpl<Invoice> invoiceIDAOImpl;
    private IDAOImpl<Parcel> parcelIDAOImpl;
    private IDAOImpl<Tires> tiresIDAOImpl;
    private IDAOImpl<Warehouse> warehouseIDAOImpl;

    // contagent

    /*
    public boolean createContragent(Contragent contragent) {
        return contragentIDAOImpl.createEntity(contragent);
    }
    */

    public Contragent getContragent(long id) {
        return contragentIDAOImpl .getEntity(id);
    }

    /*
    public Contragent updateContragent(Contragent contragent) {
        return contragentIDAOImpl.updateEntity(contragent);
    }

    public boolean deleteContragent(long id) {
        return contragentIDAOImpl.deleteEntity(id);
    }
    */

    public List<Contragent> getContragentList() {
        return contragentIDAOImpl.getEntityList();
    }

    // invoice

    /*
    public boolean createInvoice(Invoice invoice) {
        return invoiceIDAOImpl.createEntity(invoice);
    }
    */

    public Invoice getInvoice(long id) {
        return invoiceIDAOImpl.getEntity(id);
    }

    /*
    public Invoice updateInvoice(Invoice invoice) {
        return invoiceIDAOImpl.updateEntity(invoice);
    }

    public boolean deleteInvoice(long id) {
        return invoiceIDAOImpl.deleteEntity(id);
    }
    */

    public List<Invoice> getInvoiceList() {
        return invoiceIDAOImpl.getEntityList();
    }

    // parcel

    /*
    public boolean createParcel(Parcel parcel) {
        return parcelIDAOImpl.createEntity(parcel);
    }
    */

    public Parcel getParcel(long id) {
        return parcelIDAOImpl.getEntity(id);
    }

    /*
    public Parcel updateParcel(Parcel parcel) {
        return parcelIDAOImpl.updateEntity(parcel);
    }

    public boolean deleteParcel(long id) {
        return parcelIDAOImpl.deleteEntity(id);
    }

    public List<Parcel> getParcelList() {
        return parcelIDAOImpl.getEntityList();
    }
    */

    // tires

    /*
    public boolean createTires(Tires tires) {
        return tiresIDAOImpl.createEntity(tires);
    }
    */

    public Tires getTires(long id) {
        return tiresIDAOImpl.getEntity(id);
    }

    /*
    public Tires updateTires(Tires tires) {
        return tiresIDAOImpl.updateEntity(tires);
    }

    public boolean deleteTires(long id) {
        return tiresIDAOImpl.deleteEntity(id);
    }
    */

    public List<Tires> getTiresList() {
        return tiresIDAOImpl.getEntityList();
    }

    // warehouse

    /*
    public boolean createWarehouse(Warehouse warehouse) {
        return warehouseIDAOImpl.createEntity(warehouse);
    }
    */

    public Warehouse getWarehouse(long id) {
        return warehouseIDAOImpl.getEntity(id);
    }

    /*
    public Warehouse updateWarehouse(Warehouse warehouse) {
        return warehouseIDAOImpl.updateEntity(warehouse);
    }

    public boolean deleteWarehouse(long id) {
        return warehouseIDAOImpl.deleteEntity(id);
    }
    */

    public List<Warehouse> getWarehouseList() {
        return warehouseIDAOImpl.getEntityList();
    }
}
