package ua.kpi.fpm.pzks.service;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ForeignKey {
    String refTable();
    String refKey();
    ConstraintActions onUpdate() default ConstraintActions.RESTRICT;
    ConstraintActions onDelete() default ConstraintActions.RESTRICT;
}
