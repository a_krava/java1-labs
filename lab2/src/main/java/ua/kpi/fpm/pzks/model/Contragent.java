package ua.kpi.fpm.pzks.model;

import ua.kpi.fpm.pzks.service.Column;
import ua.kpi.fpm.pzks.service.Entity;
import ua.kpi.fpm.pzks.service.Primary;

import java.util.Objects;

@Entity(table = "contragents")
public class Contragent {
    @Column
    @Primary
    private long id;
    @Column
    private String name;
    @Column(name = "phone_number")
    private String phoneNumber;

    public Contragent() {}

    public Contragent(long id, String name, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contragent that = (Contragent) o;
        return id == that.id &&
                name.equals(that.name) &&
                phoneNumber.equals(that.phoneNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Contragent{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
