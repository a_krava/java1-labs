package ua.kpi.fpm.pzks;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import ua.kpi.fpm.pzks.dao.DAOImpl;
import ua.kpi.fpm.pzks.model.Contragent;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

@DisplayName("Contragent")
@ExtendWith(MockitoExtension.class)
class DAOImplContragentTest extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    void testGetContragentById() throws SQLException {
        DAOImpl<Contragent> contragentDAO = new DAOImpl<>(Contragent.class, mockConnection);
        Contragent contragentId12 = new Contragent(12,"John Doe", "+3803482863");
        when(mockResultSet.next()).thenReturn(true, false);
        when(mockResultSet.getObject(anyString())).thenAnswer(invocationOnMock -> {
            switch ((String) invocationOnMock.getArguments()[0]) {
                case "name":        return contragentId12.getName();
                case "id":          return contragentId12.getId();
                case "phoneNumber": return contragentId12.getPhoneNumber();
            }
            return null;
        });
        assertEquals(contragentId12, contragentDAO.getEntity(12));
        assertNull(contragentDAO.getEntity(4));
    }

    @Test
    @DisplayName("getEntityList")
    void testGetContragentList() throws SQLException {
        long[] ids = new long[] { 0L, 678567L };
        String[] names = new String[] { "Richard II", "Meg White" };
        String[] phoneNumbers = new String[] { "+0234965445", "+98342394" };
        DAOImpl<Contragent> contragentDAO = new DAOImpl<>(Contragent.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false, true, true, false);
        assertEquals(contragentDAO.getEntityList().size(), 0);
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countNames, countPhoneNumbers = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "name":        return names[countIds++];
                    case "id":          return ids[countNames++];
                    case "phoneNumber": return phoneNumbers[countPhoneNumbers++];
                }
                return null;
            }
        });
        var list = contragentDAO.getEntityList();
        var expectedList = new ArrayList<>() {{
            add(new Contragent(ids[0], names[0], phoneNumbers[0]));
            add(new Contragent(ids[1], names[1], phoneNumbers[1]));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
