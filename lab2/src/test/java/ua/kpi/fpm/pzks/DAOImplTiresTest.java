package ua.kpi.fpm.pzks;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import ua.kpi.fpm.pzks.dao.DAOImpl;
import ua.kpi.fpm.pzks.model.Tires;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Tires")
@ExtendWith(MockitoExtension.class)
class DAOImplTiresTest extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    void testGetTiresById() throws SQLException {
        DAOImpl<Tires> tiresDAO = new DAOImpl<>(Tires.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, false);
        Tires tiresId38 = new Tires(38,45.26, 2300, 34, 22, true);
        when(mockResultSet.getObject(anyString())).thenAnswer(invocationOnMock -> {
            switch ((String) invocationOnMock.getArguments()[0]) {
                case "id":           return tiresId38.getId();
                case "weight":       return tiresId38.getWeight();
                case "insuredValue": return tiresId38.getInsuredValue();
                case "invoiceId":    return tiresId38.getInvoiceId();
                case "radius":       return tiresId38.getRadius();
                case "forLorry":     return tiresId38.isForLorry();
            }
            return null;
        });
        assertEquals(tiresId38, tiresDAO.getEntity(38));
        assertNull(tiresDAO.getEntity(58));
    }

    @Test
    @DisplayName("getEntityList")
    void testGetTiresList() throws SQLException {
        DAOImpl<Tires> tiresDAO = new DAOImpl<>(Tires.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false, true, true, false);
        assertEquals(tiresDAO.getEntityList().size(), 0);
        Tires oneTires = new Tires(29,28.222, 345.24,
                7, 18, false);
        Tires anotherTires = new Tires(6538,13.032, 3658.03,
                382, 20, true);
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countWeights, countInsuredValues, countInvoiceIds,
                    countRadius, countForLorries = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":           return countIds++ == 0 ? oneTires.getId() : anotherTires.getId();
                    case "weight":       return countWeights++ == 0
                            ? oneTires.getWeight() : anotherTires.getWeight();
                    case "insuredValue": return countInsuredValues++ == 0
                            ? oneTires.getInsuredValue() : anotherTires.getInsuredValue();
                    case "invoiceId":    return countInvoiceIds++ == 0
                            ? oneTires.getInvoiceId() : anotherTires.getInvoiceId();
                    case "radius":       return countRadius++ == 0
                            ? oneTires.getRadius() : anotherTires.getRadius();
                    case "forLorry":     return countForLorries++ == 0
                            ? oneTires.isForLorry() : anotherTires.isForLorry();
                }
                return null;
            }
        });
        var list = tiresDAO.getEntityList();
        var expectedList = new ArrayList<>() {{ add(oneTires); add(anotherTires); }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
