package ua.kpi.fpm.pzks;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import ua.kpi.fpm.pzks.dao.DAOImpl;
import ua.kpi.fpm.pzks.model.Parcel;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Parcel")
@ExtendWith(MockitoExtension.class)
class DAOImplParcelTest extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    void testGetParcelById() throws SQLException {
        DAOImpl<Parcel> parcelDAO = new DAOImpl<>(Parcel.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, true, false);
        Parcel parcelId0 = new Parcel(0,254.275, 73.06, 253,
                247,8902, 359, null);
        Parcel parcelId1 = new Parcel(1,1625.134, 100.5, 52,
                5708, 43, 940, "some text");
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countWeights, countInsuredValues, countInvoiceIds,
                    countHeights, countWidthIds, countDepths, countDescriptions = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":           return countIds++ == 0 ? parcelId0.getId() : parcelId1.getId();
                    case "weight":       return countWeights++ == 0
                            ? parcelId0.getWeight() : parcelId1.getWeight();
                    case "insuredValue": return countInsuredValues++ == 0
                            ? parcelId0.getInsuredValue() : parcelId1.getInsuredValue();
                    case "invoiceId":    return countInvoiceIds++ == 0
                            ? parcelId0.getInvoiceId() : parcelId1.getInvoiceId();
                    case "height":       return countHeights++ == 0
                            ? parcelId0.getHeight() : parcelId1.getHeight();
                    case "width":        return countWidthIds++ == 0
                            ? parcelId0.getWidth() : parcelId1.getWidth();
                    case "depth":        return countDepths++ == 0
                            ? parcelId0.getDepth() : parcelId1.getDepth();
                    case "description":  return countDescriptions++ == 0
                            ? parcelId0.getDescription() : parcelId1.getDescription();
                }
                return null;
            }
        });
        assertEquals(parcelId0, parcelDAO.getEntity(0));
        assertEquals(parcelId1, parcelDAO.getEntity(1));
        assertNull(parcelDAO.getEntity(2));
    }

    @Test
    @DisplayName("getEntityList")
    void testGetParcelList() throws SQLException {
        DAOImpl<Parcel> parcelDAO = new DAOImpl<>(Parcel.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false, true, true, false);
        assertEquals(parcelDAO.getEntityList().size(), 0);
        Parcel oneParcel = new Parcel(2354,675.003, 491.8, 18,
                435,1093, 837, "Hi there");
        Parcel anotherParcel = new Parcel(786875,398.203, 1436.02, 95,
                815,7843, 109, "box !!! ");
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countWeights, countInsuredValues, countInvoiceIds,
                    countHeights, countWidthIds, countDepths, countDescriptions = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":           return countIds++ == 0 ? oneParcel.getId() : anotherParcel.getId();
                    case "weight":       return countWeights++ == 0
                            ? oneParcel.getWeight() : anotherParcel.getWeight();
                    case "insuredValue": return countInsuredValues++ == 0
                            ? oneParcel.getInsuredValue() : anotherParcel.getInsuredValue();
                    case "invoiceId":    return countInvoiceIds++ == 0
                            ? oneParcel.getInvoiceId() : anotherParcel.getInvoiceId();
                    case "height":       return countHeights++ == 0
                            ? oneParcel.getHeight() : anotherParcel.getHeight();
                    case "width":        return countWidthIds++ == 0
                            ? oneParcel.getWidth() : anotherParcel.getWidth();
                    case "depth":        return countDepths++ == 0
                            ? oneParcel.getDepth() : anotherParcel.getDepth();
                    case "description":  return countDescriptions++ == 0
                            ? oneParcel.getDescription() : anotherParcel.getDescription();
                }
                return null;
            }
        });
        var list = parcelDAO.getEntityList();
        var expectedList = new ArrayList<>() {{ add(oneParcel); add(anotherParcel); }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
