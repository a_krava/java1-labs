package ua.kpi.fpm.pzks;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import ua.kpi.fpm.pzks.dao.DAOImpl;
import ua.kpi.fpm.pzks.model.Warehouse;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Warehouse")
@ExtendWith(MockitoExtension.class)
class DAOImplWarehouseTest extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    void testGetWarehouseById() throws SQLException {
        DAOImpl<Warehouse> warehouseDAO = new DAOImpl<>(Warehouse.class, mockConnection);
        Warehouse warehouseId0 = new Warehouse(0,"London", "+38045634634");
        when(mockResultSet.next()).thenReturn(true, false);
        when(mockResultSet.getObject(anyString())).thenAnswer(invocationOnMock -> {
            switch ((String) invocationOnMock.getArguments()[0]) {
                case "id":          return warehouseId0.getId();
                case "address":     return warehouseId0.getAddress();
                case "phoneNumber": return warehouseId0.getPhoneNumber();
            }
            return null;
        });
        assertEquals(warehouseId0, warehouseDAO.getEntity(0));
        assertNull(warehouseDAO.getEntity(15));
    }

    @Test
    @DisplayName("getEntityList")
    void testGetWarehouseList() throws SQLException {
        DAOImpl<Warehouse> warehouseDAO = new DAOImpl<>(Warehouse.class, mockConnection);
        when(mockResultSet.next()).thenReturn(false,true, true, true, false);
        assertEquals(warehouseDAO.getEntityList().size(), 0);
        long[] ids = new long[] { 4, 23, 45};
        String[] addresses = new String[] { "bul. Bou 12, 45", "", "st. Some 12.32" };
        String[] phoneNumbers = new String[] { "+380665734", "12351", "345275"};
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countAddresses, countPhoneNumbers = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":          return ids[countIds++];
                    case "address":     return addresses[countAddresses++];
                    case "phoneNumber": return phoneNumbers[countPhoneNumbers++];
                }
                return null;
            }
        });
        var list = warehouseDAO.getEntityList();
        var expectedList = new ArrayList<>() {{
            add(new Warehouse(ids[0], addresses[0], phoneNumbers[0]));
            add(new Warehouse(ids[1], addresses[1], phoneNumbers[1]));
            add(new Warehouse(ids[2], addresses[2], phoneNumbers[2]));
        }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
