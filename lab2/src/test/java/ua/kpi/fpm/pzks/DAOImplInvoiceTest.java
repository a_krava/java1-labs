package ua.kpi.fpm.pzks;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import ua.kpi.fpm.pzks.dao.DAOImpl;
import ua.kpi.fpm.pzks.model.Invoice;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@DisplayName("Invoice")
@ExtendWith(MockitoExtension.class)
class DAOImplInvoiceTest extends DAOImplTest {
    @Test
    @DisplayName("getEntityById")
    void testGetInvoiceById() throws SQLException, ParseException {
        var p = new SimpleDateFormat("DD.MM.YY");
        long[] ids = new long[] { 23L, 7L };
        Date[] departures = new Date[] { new Date(p.parse("12.11.19").getTime()),
                new Date(p.parse("23.10.19").getTime()) };
        Date[] arrivals = new Date[] { null, new Date(p.parse("10.11.19").getTime()) };
        BigDecimal[] shippingCosts = new BigDecimal[] { new BigDecimal(234.345), new BigDecimal(24.35) };
        long[] senderIds = new long[] { 2L, 345L };
        long[] receiverIds = new long[] { 44L, 854L };
        long[] warehouseDepartureIds = new long[] { 12L, 9L };
        long[] warehouseArrivalIds = new long[] { 88L, 2L };
        DAOImpl<Invoice> invoiceDAO = new DAOImpl<>(Invoice.class, mockConnection);
        when(mockResultSet.next()).thenReturn(true, true, false);
        when(mockResultSet.getObject(anyString())).thenAnswer(new Answer() {
            private int countIds, countDepartures, countArrivals, countShippingCosts,
                    countSenderIds, countReceiverIds, countWarehouseDepartureIds,
                    countWarehouseArrivalIds = 0;
            public Object answer(InvocationOnMock invocation) {
                switch ((String) invocation.getArguments()[0]) {
                    case "id":                   return ids[countIds++];
                    case "departure":            return departures[countDepartures++];
                    case "arrival":              return arrivals[countArrivals++];
                    case "shippingCost":         return shippingCosts[countShippingCosts++];
                    case "senderId":             return senderIds[countSenderIds++];
                    case "receiverId":           return receiverIds[countReceiverIds++];
                    case "warehouseDepartureId": return warehouseDepartureIds[countWarehouseDepartureIds++];
                    case "warehouseArrivalId":   return warehouseArrivalIds[countWarehouseArrivalIds++];
                }
                return null;
            }
        });
        Invoice invoiceId23 = new Invoice(ids[0], departures[0], arrivals[0], shippingCosts[0],
                senderIds[0], receiverIds[0], warehouseDepartureIds[0], warehouseArrivalIds[0]);
        Invoice invoiceId7 = new Invoice(ids[1], departures[1], arrivals[1], shippingCosts[1],
                senderIds[1], receiverIds[1], warehouseDepartureIds[1], warehouseArrivalIds[1]);
        assertEquals(invoiceId23, invoiceDAO.getEntity(23));
        assertEquals(invoiceId7, invoiceDAO.getEntity(7));
        assertNull(invoiceDAO.getEntity(0));
    }

    @Test
    @DisplayName("getEntityList")
    void testGetInvoiceList() throws SQLException, ParseException {
        DAOImpl<Invoice> invoiceDAO = new DAOImpl<>(Invoice.class, mockConnection);
        Invoice invoice = new Invoice(548, "8.12.19", "",5.45,
                572, 1, 6, 33);
        when(mockResultSet.next()).thenReturn(false, true, false);
        assertEquals(invoiceDAO.getEntityList().size(), 0);
        when(mockResultSet.getObject(anyString())).thenAnswer(invocationOnMock -> {
            switch ((String) invocationOnMock.getArguments()[0]) {
                case "id":                   return invoice.getId();
                case "departure":            return invoice.getDeparture();
                case "arrival":              return invoice.getArrival();
                case "shippingCost":         return invoice.getShippingCost();
                case "senderId":             return invoice.getSenderId();
                case "receiverId":           return invoice.getReceiverId();
                case "warehouseDepartureId": return invoice.getWarehouseDepartureId();
                case "warehouseArrivalId":   return invoice.getWarehouseArrivalId();
            }
            return null;
        });
        var list = invoiceDAO.getEntityList();
        var expectedList = new ArrayList<>() {{ add(invoice); }};
        assertEquals(list.size(), expectedList.size());
        for (int i = 0; i < list.size(); i++) {
            assertEquals(list.get(i), expectedList.get(i));
        }
    }
}
