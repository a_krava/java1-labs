package ua.kpi.fpm.pzks.tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ua.kpi.fpm.pzks.FunctionalSet.FunctionalSetUtils;
import ua.kpi.fpm.pzks.FunctionalSet.PurelyFunctionalSet;

import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static ua.kpi.fpm.pzks.FunctionalSet.FunctionalSetUtils.*;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Functional Set Utils")
public class FunctionalSetUtilsTest {
    @Test
    @DisplayName("Empty set test")
    void emptySetTest() {
        PurelyFunctionalSet<Integer> set = empty();
        assertNotNull(set);
        assertFalse(set.contains(null));
        IntStream.range(-10, 11).forEach(x -> assertFalse(set.contains(x)));

        PurelyFunctionalSet<Character> characterSet = empty();
        assertNotNull(characterSet);
        assertFalse(characterSet.contains(null));
        assertFalse(characterSet.contains('\0'));
        assertFalse(characterSet.contains('a'));
        assertFalse(characterSet.contains('Q'));
        assertFalse(characterSet.contains('%'));
    }

    @Test
    @DisplayName("Singleton set test")
    void singletonSetTest() {
        var set = singletonSet(0);
        IntStream.range(-10, 11).forEach(x -> assertEquals(set.contains(x), x == 0));

        var stringSetOfNull = singletonSet(null);
        assertTrue(stringSetOfNull.contains(null));

        var stringSet = singletonSet("");
        assertTrue(stringSet.contains(""));

        var sb = new StringBuilder().append(this.hashCode()).insert(0, this.toString());
        var stringSetFromSb = singletonSet(sb.toString());
        assertTrue(stringSetFromSb.contains(this.toString() + this.hashCode()));

        var characterSet = singletonSet('C');
        assertTrue(characterSet.contains('C'));
        assertFalse(characterSet.contains('c'));
    }

    @Test
    @DisplayName("Union test")
    void unionTest() {
        var unionSame = union(singletonSet("TesT"), singletonSet("test"));
        assertTrue(unionSame.contains("TesT"));
        assertTrue(unionSame.contains("test"));

        int[] numbers = new int [] { -657, 34 };
        var unionNumber = union(singletonSet(numbers[0]), singletonSet(numbers[1]));
        assertTrue(unionNumber.contains(numbers[0]));
        assertTrue(unionNumber.contains(numbers[1]));

        var unionNumberAnother = union(unionNumber, singletonSet(0));
        assertTrue(unionNumberAnother.contains(0));
        assertTrue(unionNumberAnother.contains(numbers[0]));
        assertTrue(unionNumberAnother.contains(numbers[1]));

        var unionSet = convertStreamToSet(IntStream.range(-10, 11));
        IntStream.range(-10, 11).forEach(x -> assertTrue(unionSet.contains(x)));
    }

    public static <T> PurelyFunctionalSet<T> convertStreamToSet(Stream<T> stream) {
        return stream
                .map(FunctionalSetUtils::singletonSet)
                .reduce(empty(), FunctionalSetUtils::union);
    }

    public static PurelyFunctionalSet<Integer> convertStreamToSet(IntStream stream) {
        return convertStreamToSet(stream.boxed());
    }

    public static PurelyFunctionalSet<Double> convertStreamToSet(DoubleStream stream) {
        return convertStreamToSet(stream.boxed());
    }

    @Test
    @DisplayName("Intersect test")
    void intersectTest() {
        var doubleSetOne = convertStreamToSet(IntStream.range(-10, 11).mapToDouble(x -> (x - 5) / 100.0));
        var doubleSetOneAnother = convertStreamToSet(IntStream.range(-10, 11).mapToDouble(x -> (x + 5) / 100.0));
        var doubleSetIntersect = intersect(doubleSetOne, doubleSetOneAnother);
        var doubleSetIntersectReverse = intersect(doubleSetOneAnother, doubleSetOne);
        IntStream.range(-10, 11).mapToDouble(x -> x / 100.0).forEach(x -> {
            assertEquals(doubleSetIntersect.contains(x), x >= -0.05 && x <= 0.05);
            assertEquals(doubleSetIntersect.contains(x), doubleSetIntersectReverse.contains(x));
        });

        assertTrue(intersect(singletonSet("Test!"), singletonSet("Test!")).contains("Test!"));

        char[] chars = new char[] { 'Q', 'f', '0'};
        var intersectChars = intersect(
                union(singletonSet(chars[0]), singletonSet(chars[1])),
                union(singletonSet(chars[1]), singletonSet(chars[2])));
        assertTrue(intersectChars.contains(chars[1]));
        assertFalse(intersectChars.contains(chars[0]));
        assertFalse(intersectChars.contains(chars[2]));
    }

    @Test
    @DisplayName("Diff test")
    void diffTest() {
        var intSetOne = convertStreamToSet(IntStream.range(-10, 11));
        var intSetAnother = convertStreamToSet(IntStream.range(-5, 6));
        var intSetDiff = diff(intSetOne, intSetAnother);
        var intSetDiffReverse = diff(intSetAnother, intSetOne);
        IntStream.range(-10, 11).forEach(x -> {
            assertEquals(intSetDiff.contains(x), x < -5 || x > 5);
            assertFalse(intSetDiffReverse.contains(x));
        });

        var diffSet = diff(singletonSet("Some string here"), singletonSet("Another String"));
        assertTrue(diffSet.contains("Some string here"));
        assertFalse(diffSet.contains("Another String"));

        char[] chars = new char[] { 'u', 'R', '\0', 'q'};
        var charSetOne = union(singletonSet(chars[0]), singletonSet(chars[1]));
        var charSetAnother = union(singletonSet(chars[2]), singletonSet(chars[3]));
        var diffChars = diff(union(charSetOne, charSetAnother), charSetAnother);
        assertTrue(diffChars.contains(chars[1]));
        assertTrue(diffChars.contains(chars[0]));
        assertFalse(diffChars.contains(chars[2]));
        assertFalse(diffChars.contains(chars[3]));
    }

    @Test
    @DisplayName("Filter test")
    void filterTest() {
        var intSetFiltered = filter(convertStreamToSet(IntStream.range(-10, 11)), x -> x % 2 == 0);
        IntStream.range(-10, 11).forEach(x -> assertEquals(intSetFiltered.contains(x), x % 2 == 0));

        String[] strings = new String[] { "Test", "", "&@^#@", "32", "another s", "ToM" };
        var charSet = filter(convertStreamToSet(Stream.of(strings)), x -> x.length() > 3);
        Stream.of(strings).forEach(x -> assertEquals(charSet.contains(x), x.length() > 3));

        var emptySet = filter(empty(), x -> x.equals(0));
        IntStream.range(-10, 11).forEach(x -> assertFalse(emptySet.contains(x)));

        var singleSet = singletonSet(Math.PI);
        assertFalse(filter(singleSet, x -> x != Math.PI).contains(Math.PI));
        assertTrue(filter(singleSet, x -> true).contains(Math.PI));
    }
}
