package ua.kpi.fpm.pzks.tests;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static ua.kpi.fpm.pzks.tests.FunctionalSetUtilsTest.convertStreamToSet;
import static ua.kpi.fpm.pzks.FunctionalSet.FunctionalIntegerSetUtils.*;
import static ua.kpi.fpm.pzks.FunctionalSet.FunctionalSetUtils.*;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Functional Integer Set Utils")
public class FunctionalIntegerSetUtilsTest {
    @Test
    @DisplayName("ForAll test")
    void forAllTest() {
        var intSet = convertStreamToSet(IntStream.range(-10, 11));
        assertTrue(forAll(intSet, x -> x >= -10 && x < 11));
        assertFalse(forAll(intSet, x -> x != 0));

        Predicate<Integer> predicate = x -> x % 2 == 0;
        var intSetFiltered = filter(intSet, predicate);
        assertTrue(forAll(intSetFiltered, predicate));
        assertFalse(forAll(intSetFiltered, x -> !predicate.test(x)));

        Integer[] nums = new Integer[] { 100, 20, 30, 450, 990 };
        var anotherSet = convertStreamToSet(Stream.of(nums));
        assertTrue(forAll(anotherSet, x -> x > 0 && x % 10 == 0));
    }

    @Test
    @DisplayName("Exists test")
    void existsTest() {
        var intSet = convertStreamToSet(IntStream.range(-10, 11));
        assertTrue(exists(intSet, x -> x == 0));
        assertFalse(exists(intSet, x -> x == 13));
        assertFalse(exists(intSet, x -> x < -10 || x > 10));
        assertTrue(exists(intSet, x -> x >= -10 && x <= 10));

        Integer[] nums = new Integer[] { 100, 20, 30, 450, 990 };
        var anotherSet = convertStreamToSet(Stream.of(nums));
        assertTrue(exists(anotherSet, x -> x == 990));
        assertFalse(exists(anotherSet, x -> x % 2 != 0));
    }

    @Test
    @DisplayName("Map test")
    void mapTest() {
        var singleSet = singletonSet(6);
        var mappedSingleSet = map(singleSet, x -> x * 13.45);
        assertTrue(mappedSingleSet.contains(6 * 13.45));

        var mappedSet = map(convertStreamToSet(IntStream.range(-10, 11)), x -> (x + 15.20) / 100);
        IntStream.range(-10, 11).mapToDouble(x -> (x + 15.20) / 100)
                .forEach(x -> assertTrue(mappedSet.contains(x)));

        var setWithNull = map(singletonSet(10), x -> null);
        assertTrue(setWithNull.contains(null));

        var mappedHashSet = map(convertStreamToSet(IntStream.range(-10, 11)), Object::toString);
        IntStream.range(-10, 11).mapToObj(String::valueOf).forEach(x -> assertTrue(mappedHashSet.contains(x)));
    }
}
