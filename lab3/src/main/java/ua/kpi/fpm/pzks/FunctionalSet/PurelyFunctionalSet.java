package ua.kpi.fpm.pzks.FunctionalSet;

public interface PurelyFunctionalSet<T> {
    boolean contains(T element);
}
