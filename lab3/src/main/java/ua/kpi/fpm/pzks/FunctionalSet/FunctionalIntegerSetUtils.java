package ua.kpi.fpm.pzks.FunctionalSet;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;

public class FunctionalIntegerSetUtils {
    public static boolean forAll(PurelyFunctionalSet<Integer> s, Predicate<Integer> p) {
        return forAllHelper(s, p, -1000, 1000);
    }

    private static boolean forAllHelper(PurelyFunctionalSet<Integer> s, Predicate<Integer> p, int i, int stop) {
        if (i > stop) {
            return true;
        }
        if (s.contains(i) && !p.test(i)) {
            return false;
        }
        return forAllHelper(s, p, ++i, stop);
    }

    public static boolean exists(PurelyFunctionalSet<Integer> s, Predicate<Integer> p) {
        return !forAll(s, x -> !p.test(x));
    }

    public static <R> PurelyFunctionalSet<R> map(PurelyFunctionalSet<Integer> s, Function<Integer, R> p) {
        return x -> exists(s, y -> Objects.equals(p.apply(y), x));
    }
}
