package ua.kpi.fpm.pzks;

import static ua.kpi.fpm.pzks.FunctionalSet.FunctionalIntegerSetUtils.*;
import static ua.kpi.fpm.pzks.FunctionalSet.FunctionalSetUtils.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Lab #3");

        System.out.println("Check if set of (4, 5) contains 5:");
        System.out.println(union(singletonSet(4), singletonSet(5)).contains(5));

        System.out.println("Check if union contains all numbers in two sets:");
        int[] numbers = new int [] { -345, 236 };
        var unionNumbers = union(singletonSet(numbers[0]), singletonSet(numbers[1]));
        System.out.println(unionNumbers.contains(numbers[0]));
        System.out.println(unionNumbers.contains(numbers[1]));

        System.out.println("Check if \"\" is not in the set and \"-345\", \"236\" are mapped from previous set");
        var mappedSet = map(unionNumbers, Object::toString);
        System.out.println(mappedSet.contains(""));
        System.out.println(mappedSet.contains("-345"));
        System.out.println(mappedSet.contains("236"));

        System.out.println("Check if exists numbers in mapped set");
        var mappedSetAnother = map(unionNumbers, x -> x / 10);
        System.out.println(exists(mappedSetAnother, x -> x > -40 && x < -30));
    }
}
