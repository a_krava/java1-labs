package ua.kpi.fpm.pzks;

import ua.kpi.fpm.pzks.ADT.TreeNode;

import java.util.Arrays;

import static ua.kpi.fpm.pzks.ADT.Traversals.BreadthFirstTraversal;
import static ua.kpi.fpm.pzks.ADT.Traversals.DepthFirstTraversal;

public class Main {
    public static void main(String[] args) {
        System.out.println("Lab4");
        var rootNode = new TreeNode<>('F');
        var B = new TreeNode<>('B', rootNode);
        var G = new TreeNode<>('G', rootNode);
        B.addChild('A');
        var D = new TreeNode<>('D', B);
        D.addChildren(Arrays.asList('C', 'E'));
        var I = new TreeNode<>('I', G);
        G.setChildren(Arrays.asList(null, I));
        I.addChild('H');
        System.out.println("Depth first (in-order) traversal:");
        DepthFirstTraversal(rootNode).forEach(x -> System.out.print(x + " "));
        System.out.println("\n\rBreadth first traversal:");
        BreadthFirstTraversal(rootNode).forEach(x -> System.out.print(x + " "));
    }
}
