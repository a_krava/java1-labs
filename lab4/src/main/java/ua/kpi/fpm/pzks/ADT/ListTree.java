package ua.kpi.fpm.pzks.ADT;

import org.javatuples.Pair;

public class ListTree<K extends Comparable<K>, V> {
    private static final int TREEIFY_THRESHOLD = 8;
    private long count = 0;
    private RedBlackTreeNode<K, V> root = null;

    public void put(K key, V value) {
        if (!contains(key) && key != null) {
            count++;
            if (count == TREEIFY_THRESHOLD) {
                treeify();
            }
            var dataPair = new Pair<>(key, value);
            if (shouldBeTree()) {
                insertCase1(insertNodeInBST(dataPair));
            } else {
                insertNodeInList(dataPair);
            }
        }
    }

    public V get(K key) {
        var node = findNodeInListOrTree(key);
        return node == null ? null : node.getData().getValue1();
    }

    public boolean isEmpty() {
        return count == 0;
    }

    public V remove(K key) {
        var node = findNodeInListOrTree(key);
        if (node != null) {
            V valueNode = node.getData().getValue1();
            if (shouldBeTree()) {
                deleteNodeFromTree(node);
            } else {
                deleteNodeFromList(node);
            }
            if (count == TREEIFY_THRESHOLD) {
                listify();
            }
            count--;
            return valueNode;
        }
        return null;
    }

    public boolean contains(K key) {
        return findNodeInListOrTree(key) != null;
    }

    public long size() {
        return count;
    }

    private boolean shouldBeTree() {
        return count >= TREEIFY_THRESHOLD;
    }

    private Pair<RedBlackTreeNode<K, V>, RedBlackTreeNode<K, V>> findNodeAndAncestorInListOrTree(
            K expectedKey, RedBlackTreeNode<K, V> curNode, RedBlackTreeNode<K, V> prevNode) {
        if (curNode != null && curNode.getData() != null && expectedKey != null) {
            var actualKey = curNode.getData().getValue0();
            if (expectedKey.equals(actualKey)) {
                return new Pair<>(curNode, prevNode);
            } else if (shouldBeTree() && expectedKey.compareTo(actualKey) < 0) {
                return findNodeAndAncestorInListOrTree(expectedKey, curNode.getLeftChild(), curNode);
            } else {
                return findNodeAndAncestorInListOrTree(expectedKey, curNode.getRightChild(), curNode);
            }
        }
        return new Pair<>(null, prevNode);
    }

    private RedBlackTreeNode<K, V> findNodeInListOrTree(K expectedKey) {
        return findNodeAndAncestorInListOrTree(expectedKey, root, null).getValue0();
    }

    private void insertNodeInList(Pair<K, V> data) {
        var node = new RedBlackTreeNode<>(data);
        node.setRightChild(root);
        if (root != null) {
            root.setParent(node);
        }
        root = node;
    }

    private RedBlackTreeNode<K, V> insertNodeInBST(Pair<K, V> data) {
        var pair = findNodeAndAncestorInListOrTree(data.getValue0(), root, null);
        var parent = pair.getValue1();
        var newNode = new RedBlackTreeNode<>(data);
        if (parent == null) {
            root = newNode;
        } else {
            newNode.setParent(parent);
            if (data.getValue0().compareTo(parent.getData().getValue0()) < 0) {
                parent.setLeftChild(newNode);
            } else {
                parent.setRightChild(newNode);
            }
        }
        return newNode;
    }

    private void deleteNodeFromList(RedBlackTreeNode<K, V> node) {
        var parent = (RedBlackTreeNode<K, V>) node.getParent();
        var child = node.getRightChild();
        node.setParent(null);
        node.setRightChild(null);
        if (parent == null) {
            root = child;
            if (root != null) {
                root.setParent(null);
            }
        } else {
            parent.setRightChild(child);
            if (child != null) {
                child.setParent(parent);
            }
        }
    }

    private void deleteNodeFromTree(RedBlackTreeNode<K, V> node) {
        if (node.getLeftChild() != null && node.getLeftChild().getData() != null
                && node.getRightChild() != null && node.getRightChild().getData() != null) {
            var minimumNode = getMinimumNodeHelper(node.getRightChild());
            node.setData(minimumNode.getData());
            deleteOneChild(minimumNode);
        } else {
            deleteOneChild(node);
        }
    }

    private RedBlackTreeNode<K, V> getMinimumNodeHelper(RedBlackTreeNode<K, V> node) {
        if (node != null && node.getData() != null) {
            var leftNode = node.getLeftChild();
            if (leftNode == null || leftNode.getData() == null) {
                return node;
            } else {
                return getMinimumNodeHelper(leftNode);
            }
        }
        return null;
    }

    private void listify() {
        var temp = root;
        root = null;
        listifyHelper(temp);
    }

    private void listifyHelper(RedBlackTreeNode<K, V> node) {
        if (node != null) {
            var pair = node.getData();
            if (pair != null) {
                insertNodeInList(new Pair<>(pair.getValue0(), pair.getValue1()));
            }
            for (var subNode : node) {
                listifyHelper((RedBlackTreeNode<K, V>) subNode);
            }
        }
    }

    private void treeify() {
        var temp = root;
        root = null;
        treeifyHelper(temp);
    }

    private void treeifyHelper(RedBlackTreeNode<K, V> node) {
        if (node != null) {
            var pair = node.getData();
            insertCase1(insertNodeInBST(new Pair<>(pair.getValue0(), pair.getValue1())));
            treeifyHelper(node.getRightChild());
        }
    }

    private void insertCase1(RedBlackTreeNode<K, V> node) {
        if (node.getParent() == null) {
            node.setColor(RedBlackTreeNode.BLACK);
        } else {
            insertCase2(node);
        }
    }

    private void insertCase2(RedBlackTreeNode<K, V> node) {
        var parent = (RedBlackTreeNode<K, V>) node.getParent();
        if (parent.isRed()) {
            insertCase3(node);
        }
    }

    private void insertCase3(RedBlackTreeNode<K, V> node) {
        var uncle = node.getUncle();
        if (uncle != null && uncle.isRed()) {
            var parent = (RedBlackTreeNode<K, V>) node.getParent();
            var grandparent = node.getGrandparent();
            parent.setColor(RedBlackTreeNode.BLACK);
            uncle.setColor(RedBlackTreeNode.BLACK);
            grandparent.setColor(RedBlackTreeNode.RED);
            insertCase1(grandparent);
        } else {
            insertCase4(node);
        }
    }

    private void insertCase4(RedBlackTreeNode<K, V> node) {
        var grandparent = node.getGrandparent();
        var parent = (RedBlackTreeNode<K, V>) node.getParent();
        if (node.equals(parent.getRightChild()) && parent.equals(grandparent.getLeftChild())) {
            rotateLeft(parent);
            node = node.getLeftChild();
        } else if (node.equals(parent.getLeftChild()) &&  parent.equals(grandparent.getRightChild())) {
            rotateRight(parent);
            node = node.getRightChild();
        }
        insertCase5(node);
    }

    private void insertCase5(RedBlackTreeNode<K, V> node) {
        var grandparent = node.getGrandparent();
        var parent = (RedBlackTreeNode<K, V>) node.getParent();
        parent.setColor(RedBlackTreeNode.BLACK);
        grandparent.setColor(RedBlackTreeNode.RED);
        if (node.equals(parent.getLeftChild()) && parent.equals(grandparent.getLeftChild())) {
            rotateRight(grandparent);
        } else {
            rotateLeft(grandparent);
        }
    }

    private void replaceNode(RedBlackTreeNode<K, V> node, RedBlackTreeNode<K, V> child) {
        var parent = (RedBlackTreeNode<K, V>) node.getParent();
        child.setParent(parent);
        if (node.equals(parent.getLeftChild())) {
            parent.setLeftChild(child);
        } else {
            parent.setRightChild(child);
        }
    }

    private void deleteOneChild(RedBlackTreeNode<K, V> node) {
        var child = (node.getRightChild() == null || node.getRightChild().getData() == null)
                ? node.getLeftChild() : node.getRightChild();
        replaceNode(node, child);
        if (!node.isRed()) {
            if (child.isRed()) {
                child.setColor(RedBlackTreeNode.BLACK);
            } else {
                deleteCase1(child);
            }
        }
    }

    private void deleteCase1(RedBlackTreeNode<K, V> node) {
        if (node.getParent() != null) {
            deleteCase2(node);
        }
    }

    private void deleteCase2(RedBlackTreeNode<K, V> node) {
        var sibling = node.getSibling();
        var parent = (RedBlackTreeNode<K, V>) node.getParent();
        if (sibling.isRed()) {
            parent.setColor(RedBlackTreeNode.RED);
            sibling.setColor(RedBlackTreeNode.BLACK);
            if (node.equals(parent.getLeftChild())) {
                rotateLeft(parent);
            } else {
                rotateRight(parent);
            }
        }
        deleteCase3(node);
    }

    private void deleteCase3(RedBlackTreeNode<K, V> node) {
        var sibling = node.getSibling();
        var parent = (RedBlackTreeNode<K, V>) node.getParent();
        if (!parent.isRed() && !sibling.isRed()
                && (sibling.getLeftChild() == null || !sibling.getLeftChild().isRed())
                && (sibling.getLeftChild() == null || !sibling.getRightChild().isRed())) {
            sibling.setColor(RedBlackTreeNode.RED);
            deleteCase1(parent);
        } else {
            deleteCase4(node);
        }
    }

    private void deleteCase4(RedBlackTreeNode<K, V> node) {
        var sibling = node.getSibling();
        var parent = (RedBlackTreeNode<K, V>) node.getParent();
        if (parent.isRed() && !sibling.isRed()
                && (sibling.getLeftChild() == null || !sibling.getLeftChild().isRed())
                && (sibling.getRightChild() == null || !sibling.getRightChild().isRed())) {
            sibling.setColor(RedBlackTreeNode.RED);
            parent.setColor(RedBlackTreeNode.BLACK);
        } else {
            deleteCase5(node);
        }
    }

    private void deleteCase5(RedBlackTreeNode<K, V> node) {
        var sibling = node.getSibling();
        var parent = (RedBlackTreeNode<K, V>) node.getParent();
        if (!sibling.isRed()) {
            if (node.equals(parent.getLeftChild())
                    && !sibling.getRightChild().isRed() && sibling.getLeftChild().isRed()) {
                sibling.setColor(RedBlackTreeNode.RED);
                sibling.getLeftChild().setColor(RedBlackTreeNode.BLACK);
                rotateRight(sibling);
            } else if (node.equals(parent.getRightChild())
                    && !sibling.getLeftChild().isRed() && sibling.getRightChild().isRed()) {
                sibling.setColor(RedBlackTreeNode.RED);
                sibling.getRightChild().setColor(RedBlackTreeNode.BLACK);
                rotateLeft(sibling);
            }
        }
        deleteCase6(node);
    }

    private void deleteCase6(RedBlackTreeNode<K, V> node) {
        var sibling = node.getSibling();
        var parent = (RedBlackTreeNode<K, V>) node.getParent();
        sibling.setColor(parent.isRed());
        parent.setColor(RedBlackTreeNode.BLACK);
        if (node.equals(parent.getLeftChild())) {
            sibling.getRightChild().setColor(RedBlackTreeNode.BLACK);
            rotateLeft(parent);
        } else {
            sibling.getLeftChild().setColor(RedBlackTreeNode.BLACK);
            rotateRight(parent);
        }
    }

    private void rotateLeft(RedBlackTreeNode<K, V> node) {
        var pivot = node.getRightChild();
        var parent = (RedBlackTreeNode<K, V>) node.getParent();
        pivot.setParent(parent);
        if (root.equals(node)) {
            root = pivot;
        }
        changeParentChild(pivot, node, parent);
        node.setRightChild(pivot.getLeftChild());
        pivot.getLeftChild().setParent(node);
        node.setParent(pivot);
        pivot.setLeftChild(node);
    }

    private void rotateRight(RedBlackTreeNode<K, V> node) {
        var pivot = node.getLeftChild();
        var parent = (RedBlackTreeNode<K, V>) node.getParent();
        pivot.setParent(parent);
        if (root.equals(node)) {
            root = pivot;
        }
        changeParentChild(pivot, node, parent);
        node.setLeftChild(pivot.getRightChild());
        pivot.getRightChild().setParent(node);
        node.setParent(pivot);
        pivot.setRightChild(node);
    }

    private static <K, V> void changeParentChild(
            RedBlackTreeNode<K, V> pivot, RedBlackTreeNode<K, V> node, RedBlackTreeNode<K, V> parent) {
        if (parent != null) {
            if (parent.getLeftChild().equals(node)) {
                parent.setLeftChild(pivot);
            } else {
                parent.setRightChild(pivot);
            }
        }
    }
}
