package ua.kpi.fpm.pzks.ADT;

import org.javatuples.Pair;

import java.util.*;

public class RedBlackTreeNode<K, V> extends TreeNode<Pair<K, V>> {
    public static final boolean RED = true;
    public static final boolean BLACK = false;
    private boolean isRed;

    public RedBlackTreeNode(RedBlackTreeNode<K, V> parent) {
        super(null);
        isRed = false;
        setParent(parent);
        super.setChildren(new LinkedList<>(Arrays.asList(null, null)));
    }

    public RedBlackTreeNode(Pair<K, V> data) {
        super(data);
        isRed = true;
        var dummyNode = new RedBlackTreeNode<>(this);
        super.setChildren(new LinkedList<TreeNode<Pair<K, V>>>(Arrays.asList(dummyNode, dummyNode)));
    }

    public boolean isRed() {
        return isRed;
    }

    public void setColor(boolean isRed) {
        this.isRed = isRed;
    }

    public RedBlackTreeNode<K, V> getLeftChild() {
        return (RedBlackTreeNode<K, V>) getChild(0);
    }

    public RedBlackTreeNode<K, V> getRightChild() {
        return (RedBlackTreeNode<K, V>) getChild(1);
    }

    public void setLeftChild(TreeNode<Pair<K, V>> child) {
        setChild(0, child);
    }

    public void setRightChild(TreeNode<Pair<K, V>> child) {
        setChild(1, child);
    }

    public RedBlackTreeNode<K, V> getGrandparent() {
        var parent = getParent();
        if (parent != null) {
            return (RedBlackTreeNode<K, V>) parent.getParent();
        } else {
            return null;
        }
    }

    public RedBlackTreeNode<K, V> getUncle() {
        var grandparent = getGrandparent();
        var parent = getParent();
        if (grandparent != null) {
            if (parent.equals(grandparent.getLeftChild())) {
                return grandparent.getRightChild();
            } else {
                return grandparent.getLeftChild();
            }
        } else {
            return null;
        }
    }

    public RedBlackTreeNode<K, V> getSibling() {
        var parent = (RedBlackTreeNode<K, V>) getParent();
        if (parent == null) {
            return null;
        } else if (this.equals(parent.getLeftChild())) {
            return parent.getRightChild();
        } else {
            return parent.getLeftChild();
        }
    }

    @Override
    public void setChildren(List<? extends TreeNode<Pair<K, V>>> children) {
        if (children.size() == this.getChildren().size()) {
            super.setChildren(children);
        }
    }

    @Override
    public void addChildren(Collection<Pair<K, V>> children) { }

    @Override
    public void addChild(Pair<K, V> child) { }
}
