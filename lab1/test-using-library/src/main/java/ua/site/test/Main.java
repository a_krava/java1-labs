package ua.site.test;

import org.javatuples.Pair;
import ua.kpi.fpm.pzks.Circle;

public class Main {
    public static void main(String[] args) {
        Circle c1 = new Circle(5);
        Circle c2 = new Circle(2.5, -3.8, 1);
        Circle c3 = new Circle(new Pair<>(1.0, 2.5), 4);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
        System.out.println(c1.calculateSquare());
    }
}
