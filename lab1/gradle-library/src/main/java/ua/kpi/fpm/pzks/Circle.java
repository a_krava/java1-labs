package ua.kpi.fpm.pzks;

import org.javatuples.Pair;



public class Circle {
    private int radius;
    private Pair<Double, Double> center;

    public Circle(int radius) {
        this.radius = radius;
        this.center = new Pair<>(0.0, 0.0);
    }

    public Circle(double a, double b, int radius) {
        this.radius = radius;
        this.center = new Pair<>(a, b);
    }

    public Circle(Pair<Double, Double> center, int radius) {
        this.radius = radius;
        this.center = center;
    }

    public double length() {
        return Math.PI * 2 * radius;
    }


    public double calculateSquare() {
        return Math.PI * Math.pow(this.radius, 2);
    }

    @Override
    public String toString() {
        return "Circle-gradle [radius=" + radius + ", center=" + center + "]";
    }
}