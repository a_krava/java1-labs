package ua.example.test;

import org.javatuples.Pair;
import ua.kpi.fpm.pzks.Circle;

public class Main {
    public static void main(String[] args) {
        System.out.println("gradle");
        Circle c1 = new Circle(7);
        Circle c4 = new Circle(new Pair<>(1.7, 5.0), 7);
        System.out.println(c4);
        System.out.println("length: " + c1.length());
    }
}
